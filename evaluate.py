import dvc.api
from dvclive import Live

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.inspection import permutation_importance
from joblib import load

from sklearn.metrics import (
    accuracy_score,
    recall_score,
    precision_score,
    f1_score,
    roc_auc_score,
    precision_recall_curve,
    average_precision_score,
)


def calculate_metrics(target_test, probabilities):
    predictions = probabilities > 0.5
    ap = average_precision_score(target_test, probabilities)
    roc_auc = roc_auc_score(target_test, probabilities)

    precision, recall, thresholds = precision_recall_curve(
        target_test,
        probabilities,
    )
    # You may adjust the beta here:
    beta = 1
    f_scores = (
        (1 + beta**2)
        * recall
        * precision
        / (recall + beta**2 * precision + np.finfo(float).eps)
    )
    best_thresh = thresholds[np.argmax(f_scores)]
    best_f = np.max(f_scores)

    optimal_threshold_preds = probabilities > best_thresh

    perm_importances = pd.DataFrame(
        {
            "token": vectorizer.get_feature_names_out(),
            "importance": permutation_importance(
                model,
                X_test,
                y_test.values,
                random_state=dvc.api.params_show().get("random_state"),
                scoring="neg_log_loss",
                n_jobs=-1,
            )["importances_mean"],
        }
    )

    with Live(report="html", cache_images=False, dvcyaml=None) as live:
        cv_logloss = load("model/logloss.joblib")
        cv_brier = load("model/brier.joblib")
        live.log_metric("CV Log loss", cv_logloss.mean(), plot=False)
        live.log_metric("CV Brier score", cv_brier.mean(), plot=False)
        live.log_metric(
            "Accuracy @ 0.5",
            accuracy_score(target_test, predictions),
            plot=False,
        )
        live.log_metric(
            "Accuracy (optimal threshold)",
            accuracy_score(target_test, optimal_threshold_preds),
            plot=False,
        )
        live.log_metric(
            "Recall @ 0.5", recall_score(target_test, predictions), plot=False
        )
        live.log_metric(
            "Recall (optimal threshold)",
            recall_score(target_test, optimal_threshold_preds),
            plot=False,
        )
        live.log_metric(
            "Precision @ 0.5",
            precision_score(target_test, predictions),
            plot=False,
        )
        live.log_metric(
            "Precision (optimal threshold)",
            precision_score(target_test, optimal_threshold_preds),
            plot=False,
        )
        live.log_metric("ROC_AUC", roc_auc, plot=False)
        live.log_metric("AP (PR_AUC)", ap, plot=False)
        live.log_metric(
            "F1 @ 0.5", f1_score(target_test, predictions), plot=False
        )
        live.log_metric("F1 (optimal threshold)", best_f, plot=False)
        live.log_metric("Optimal threshold", best_thresh, plot=False)
        live.log_sklearn_plot(
            "roc", target_test, probabilities, drop_intermediate=True
        )
        live.log_sklearn_plot(
            "precision_recall",
            target_test,
            probabilities,
            drop_intermediate=True,
        )
        live.log_sklearn_plot(
            "confusion_matrix",
            target_test,
            predictions,
            name="cmatrix_default",
        )
        live.log_sklearn_plot(
            "confusion_matrix",
            target_test,
            optimal_threshold_preds,
            name="cmatrix_optimal",
        )
        live.log_plot(
            "Feature importance",
            perm_importances.sort_values(
                by="importance", ascending=False
            ).head(10),
            x="importance",
            y="token",
            template="bar_horizontal_sorted",
            title="Most important tokens",
            y_label="Token",
            x_label="Importance",
        )

        fig, ax = plt.subplots()
        ax.boxplot(cv_logloss, vert=False)
        plt.title("Cross-validation: Log Loss")
        live.log_image("Log Loss.png", fig)
        fig, ax = plt.subplots()
        ax.boxplot(cv_brier, vert=False)
        plt.title("Cross-validation: Brier Score")
        live.log_image("Brier Score.png", fig)


vectorizer = load("model/vectorizer.joblib")
model = load("model/model.joblib")

val_data = pd.read_csv("data/test.csv")

X_test = vectorizer.transform(val_data["full_text"]).toarray()
y_test = val_data["label"]

probs = model.predict_proba(X_test)[:, 1]

calculate_metrics(y_test, probs)
