import pandas as pd
import dvc.api

from sklearn.preprocessing import Normalizer
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.pipeline import Pipeline
from joblib import dump

vectorizer_method = dvc.api.params_show().get("vectorizer_method")
scaler_method = dvc.api.params_show().get("scaler_method")
params = dvc.api.params_show().get("vectorizer")
ngram_range = tuple(dvc.api.params_show().get("ngram_range"))

model_map = {
    "bow": CountVectorizer,
    "tfidf": TfidfVectorizer,
}

# QA: very few methods work with sparse matrices.
scale_map = {
    "length": Normalizer(),
}

vectorizer = Pipeline(
    [
        (
            "vectorize",
            model_map.get(vectorizer_method)(
                stop_words="english", ngram_range=ngram_range, **params
            ),
        ),
        ("scale", scale_map.get(scaler_method, "length")),
    ]
)

train_data = pd.read_csv("data/train.csv")

vectorizer.fit(train_data["full_text"])

dump(vectorizer, "model/vectorizer.joblib")
