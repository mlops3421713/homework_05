FROM continuumio/miniconda3

WORKDIR /app

# Creating environment
COPY dev.yaml .
RUN conda env create -f dev.yaml

# Copying project settings
COPY pyproject.toml .
